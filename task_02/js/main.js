(function () {
	var classNames = {
		main: 'statistics__main',
		sub: 'statistics__sub',
		stateActive: '_state_active'
	};
	
	var triggers = document.querySelectorAll('.'+classNames.main);

	for (var index = 0; index < triggers.length; index++) {
		var trigger = triggers[index];

		trigger.addEventListener('click', function () {
			var main = this;
			var siblingElement = main.nextElementSibling;

			main.classList.toggle(classNames.main+classNames.stateActive);

			while (siblingElement) {
				if (siblingElement.classList.contains(classNames.main)) break;

				if (siblingElement.classList.contains(classNames.sub)) {
					siblingElement.classList.toggle(classNames.sub+classNames.stateActive)
				}
				
				siblingElement = siblingElement.nextElementSibling;
			}
		})
	}
})();